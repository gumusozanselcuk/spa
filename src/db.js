const mongoose = require('mongoose');

async function connect(username, password, dbname) {
    try {
        await mongoose.connect('mongodb://mongodb:27017/' + dbname,
            {
                useNewUrlParser: true,
                useUnifiedTopology: true
            }
        );
    } catch (error) {
        console.error(error);
    }
}

module.exports = {connect}
