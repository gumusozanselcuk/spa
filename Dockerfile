FROM node:16

COPY package*.json ./
RUN rm -rf node_modules package-lock.json
COPY ["package.json", "package-lock.json*", "./"]
RUN apt-get -yq update && apt-get -yq install git bzip2 automake build-essential
RUN npm install

COPY . .

EXPOSE 3000
CMD [ "node", "server.js" ]