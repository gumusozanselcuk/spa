const express = require('express');
const app = express();
const phasesRoutes = require('./routes/phase.server.routes');
const tasksRoutes = require('./routes/task.server.routes');
const bodyParser = require('body-parser');

const db = require('./src/db');
db.connect('','','StartupProgressAppDB');

app.use(bodyParser.json());

app.use('/phases', phasesRoutes);
app.use('/tasks', tasksRoutes);

app.listen(3000);
