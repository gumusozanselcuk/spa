const Task = require('../models/task.server.model');
const Phase = require('../models/phase.server.model');

const getTasks = async (req, res) => {
    try {
        const tasks = await Task.find();
        res.status(200).json(tasks);
    } catch (err){
        res.status(500).json({err});
    }
}

const updateTask = async (req, res) => {
    let filter = {name: req.body.taskName, phaseName: req.body.phaseName};
    let update = res.updatedTask;
    try {
        const updatedTask = await Task.findOneAndUpdate(filter, update);
        res.status(200).json(updatedTask);
    } catch (err){
        res.status(500).json({err});
    }
}

//remove task and control all the remaining tasks are done.
const removeTask = async (req, res) => {
    let filter = {name: req.body.taskName, phaseName: req.body.phaseName};
    try {
        const removedTask = await Task.remove(filter);
        await controlIsPhaseDone(req.body.phaseName);
        res.status(200).json(removedTask);
    } catch (err){
        res.status(500).json({err});
    }
}

//after new task is created, add it to related phase
const updatePhaseForPostedTask = async (phaseName, task) => {
    let phaseQueryFilter = {name: phaseName};
    let phaseQueryUpdate = {$push: {tasks: task}};
    await Phase.findOneAndUpdate(phaseQueryFilter, phaseQueryUpdate);
}

//create new task (if a task with same name and same phase name not exists)
const postTask = async (req, res) => {
    const task = new Task();
    task.name = req.body.name;
    task.phaseName = req.body.phaseName;

    const queriedTaskExists = await Task.exists({name:task.name, phaseName: task.phaseName});

    if(queriedTaskExists)
        res.status(500).json('Task Name: ' + task.name + ' exists in Phase: ' + task.phaseName);
    else {
        try {
            task.isDone = false;
            task.createdTime = new Date();
            await task.save();
            await updatePhaseForPostedTask(req.body.phaseName, task);

            res.status(200).json('Task:' + task.name + ' successfully saved!');
        } catch (err) {
            res.status(500).json({err});
        }
    }
}

//update task statuses(checkbox statuses,done or not) in the Phase card, and control Phase status will be done
const updateTasksStatuses = async (req, res) => {
    const phaseName = req.body.phaseName;
    const updatedTasks = req.body.updatedTasks;

    try{
        const taskPromiseList = updatedTasks.map(task => findAndUpdatedTasks(task))
        await Promise.all(taskPromiseList);
        await controlIsPhaseDone(phaseName);

        res.status(200).json('Tasks successfully updated!');
    }catch(err){
        res.status(500).json({err});
    }
}

//control all tasks are marked as done or not in the related Phase
const controlIsPhaseDone = async (phaseName) => {
    const phaseTasks = await Task.find({phaseName: phaseName});

    let allTasksIsDone = true;
    phaseTasks.forEach(task => {
        if(!task.isDone)
            allTasksIsDone = false;
    })

    let isDone=false;

    if(allTasksIsDone)
        isDone = true;

    await Phase.updateOne({name:phaseName}, {isDone: isDone, updatedTime: new Date()})
}

//update task status
const findAndUpdatedTasks = (task) => {
    let filterObject = {name: task.name, phaseName: task.phaseName};
    let updateObject = {isDone: task.isDone, updatedTime: new Date()};
    return Task.findOneAndUpdate(filterObject,updateObject);
}

module.exports = {getTasks,postTask,updateTask,removeTask, updateTasksStatuses}
