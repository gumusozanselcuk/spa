const Phase = require('../models/phase.server.model');
const Task = require('../models/task.server.model');

const getPhases = async (req, res) => {
    try {
        const phases = await Phase.find().sort({'phaseNumber': 1})
            .populate({ path: 'tasks', model: Task, options: {sort: { createdTime: 1 }}});
        res.status(200).json(phases);
    } catch(err){
        res.status(500).json({err});
    }
}

//create new phase (if a phase with same name not exists)
const postPhase = async (req, res) => {
    const queriedPhaseNameExists = await Phase.exists({
        name: req.body.name
    });

    const queriedPhaseNumberExists = await Phase.exists({
        phaseNumber: req.body.phaseNumber
    });

    if(queriedPhaseNameExists)
        res.status(500).json('Phase Name: ' + req.body.name + ' exists');
    else {
        try {
            const phase = new Phase({
                name: req.body.name,
                phaseNumber: req.body.phaseNumber,
                isDone: false,
                createdTime: new Date()
            })

            if(queriedPhaseNumberExists)
                await Phase.updateMany({ phaseNumber: { $gte: req.body.phaseNumber} }, {$inc : {phaseNumber : 1}})

            await phase.save();
            res.status(200).json('Phase:' + phase.name + ' successfully saved!');
        } catch (err) {
            res.status(500).json({err});
        }
    }
}

const updatePhase = async (req, res) => {
    let filter = {phaseName: req.body.phaseName};
    let update = res.updatedPhase;
    try {
        const updatedPhase = await Phase.findOneAndUpdate(filter, update);
        res.json(updatedPhase);
    } catch (err){
        res.status(500).json({err});
    }
}

const removePhase = async (req, res) => {
    let filter = {phaseName: req.body.phaseName};
    try {
        const removedPhase = await Phase.remove(filter);
        res.json(removedPhase);
    } catch (err){
        res.status(500).json({err});
    }
}

module.exports = {getPhases,postPhase,updatePhase,removePhase}
