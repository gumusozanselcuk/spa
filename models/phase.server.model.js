const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const PhaseSchema = new Schema({
    name:{
        type: String,
    },
    isDone:{
        type: Boolean
    },
    phaseNumber: {
        type: Number
    },
    tasks: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: "Task"
        }
    ],
    createdTime: {
        type: Date
    },
    updatedTime: {
        type: Date
    }
})

module.exports = mongoose.model('Phases', PhaseSchema);
