const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const TaskSchema = new Schema({
    name: {
        type: String
    },
    isDone: {
        type: Boolean
    },
    phaseName: {
        type: String
    },
    createdTime: {
        type: Date
    },
    updatedTime: {
        type: Date
    }
})

module.exports = mongoose.model('Tasks', TaskSchema);
