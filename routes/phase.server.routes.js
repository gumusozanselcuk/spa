const express = require('express');
const router = express.Router();
const phaseController = require('../controllers/phase.server.controller');

router.get('/', phaseController.getPhases)
router.post('/', phaseController.postPhase);
router.put('/',phaseController.updatePhase)
router.delete('/',phaseController.removePhase)

module.exports = router;