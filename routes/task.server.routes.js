const express = require('express');
const router = express.Router();
const taskController = require('../controllers/task.server.controller')

router.get('/', taskController.getTasks)
router.post('/', taskController.postTask)
router.post('/updateTasksStatuses', taskController.updateTasksStatuses)
router.put('/',taskController.updateTask)
router.delete('/',taskController.removeTask)

module.exports = router;